@extends('layout.master')

@section('title')
    Halaman form
@endsection

@section('content')
    <h1>Buat Account Baru</h1>
    <h3>Sign Up Form</h3>
    <form action="/daftar" method="post">
        @csrf
        <label for="firstname">First Name: </label><br>
        <input type="text" name="firstname" id="firstname"><br><br>

        <label for="lastname">Last Name:</label><br>
        <input type="text" name="lastname" id="lastname"><br><br>

        <label>Gender:</label><br>
        <input type="radio" name="gender">Male<br>
        <input type="radio" name="gender">Famale<br>
        <input type="radio" name="gender">Other<br><br>

        <label>Nationally:</label>
        <select name="" id="">
            <option value="">Indonesia</option>
            <option value="">Malaysia</option>
            <option value="">Others</option>
        </select><br><br>

        <label>Languages Spoken</label><br>
        <input type="checkbox">Bahasa Indonesia<br>
        <input type="checkbox">English<br>
        <input type="checkbox">Other<br><br>

        <label for="bio">Bio:</label><br>
        <textarea name="" id="bio" cols="30" rows="10"></textarea><br>

        <input type="submit" value="Sign Up">
    </form>
@endsection